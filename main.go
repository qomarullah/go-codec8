package main

import (
	"bufio"
	"fmt"
	"net"
	"os"

	"gitlab.com/qomarullah/go-codec8/decoder"
)

const (
	CONN_HOST = "0.0.0.0"
	CONN_PORT = "1883"
	CONN_TYPE = "tcp"
)

type Element struct {
	No              string `json:"No"`
	Propertyname    string `json:"PropertyName"`
	Bytes           string `json:"Bytes"`
	Type            string `json:"Type"`
	Min             string `json:"Min"`
	Max             string `json:"Max"`
	Multiplier      string `json:"Multiplier"`
	Units           string `json:"Units"`
	Description     string `json:"Description"`
	Hwsupport       string `json:"HWSupport"`
	ParametrGroup   string `json:"Parametr Group"`
	Finalconversion string `json:"FinalConversion"`
}

func main() {

	//testing
	message := &decoder.Message{}
	data := "00000000000004d1080f0000017862d70510003fbbff81fc431e9c002101010a0000001107ef01f0001505c80045010201715906b50006b60005424fda430fa04400000913ba04f10000c7421000018a9748000001310500000000000000017862d708f8003fbbff81fc431e9c00210101090000001107ef01f0001505c80045010201715906b50007b60005424fdc430fa04400000913bd04f10000c7421000018a9748000001310500000000000000017862d70ce0003fbbff81fc431e9c00210101090000001107ef01f0001505c80045010201715906b50007b60005424fdd430fa04400000913be04f10000c7421000018a9748000001310500000000000000017862d710c8003fbbff81fc431e9c00210101090000001107ef01f0001505c80045010201715906b50007b60005424fdc430fa04400000913bc04f10000c7421000018a9748000001310500000000000000017862d714b0003fbbff81fc431e9c002101010a0000001107ef01f0001505c80045010201715906b50006b60005424fdf430fa04400000913c204f10000c7421000018a9748000001310500000000000000017862d71898003fbbff81fc431e9c002101010a0000001107ef01f0001505c80045010201715906b50006b60005424fdc430fa04400000913b704f10000c7421000018a9748000001310500000000000000017862d71c80003fbbff81fc431e9c002101010a0000001107ef01f0001505c80045010201715906b50006b60005424fd8430fa04400000913bc04f10000c7421000018a9748000001310500000000000000017862d72068003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60005424fd8430fa04400000913bd04f10000c7421000018a9748000001310500000000000000017862d72450003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60005424fd7430fa04400000913be04f10000c7421000018a9748000001310500000000000000017862d72838003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424fde430fa04400000913c304f10000c7421000018a9748000001310500000000000000017862d72c20003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424fe2430fa04400000913b904f10000c7421000018a9748000001310500000000000000017862d73008003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424feb430fa04400000913ba04f10000c7421000018a9748000001310500000000000000017862d733f0003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424fde430fa04400000913c404f10000c7421000018a9748000001310500000000000000017862d737d8003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424fec430fa04400000913c704f10000c7421000018a9748000001310500000000000000017862d73bc0003fbbff81fc431e9c002101010b0000001107ef01f0001505c80045010201715906b50006b60004424fe8430fa04400000913c604f10000c7421000018a9748000001310500000000000f00003550"
	decoder.Decode(data, message)

	// Listen for incoming connections.
	l, err := net.Listen(CONN_TYPE, CONN_HOST+":"+CONN_PORT)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	fmt.Println("Listening on " + CONN_HOST + ":" + CONN_PORT)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	defer conn.Close()
	message := &decoder.Message{}
	for {
		// Make a buffer to hold incoming data.
		buf := make([]byte, 2048)
		// Read the incoming connection into the buffer.
		reqLen, err := conn.Read(buf)
		if err != nil {
			fmt.Println("Error reading:", err.Error())
			conn.Close()
			fmt.Println("close connection..")
			return
		}

		data := fmt.Sprintf("%x", buf[:reqLen])
		fmt.Println("data:", data)

		if len(data) == 17 {

			response := "01"
			w := bufio.NewWriter(conn)
			w.Write([]byte(response))
			w.Flush()
			message.Imei = data

		} else {

			response := data[len(data)-10 : len(data)-8]
			go decoder.Decode(data, message)
			fmt.Println("response:", response)
			w := bufio.NewWriter(conn)
			w.Write([]byte(response))
			w.Flush()
			break

		}

	}
}
