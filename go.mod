module gitlab.com/qomarullah/go-codec8

go 1.15

require (
	github.com/filipkroca/b2n v0.0.0-20190805132448-22fb58c69d13 // indirect
	github.com/filipkroca/teltonikaparser v0.0.0-20210320104439-13ce2332f5d7
	github.com/spf13/cast v1.3.1
)
